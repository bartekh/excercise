**Atos excercise**  
This library represents "real" library. State is kept in memory.  
Functionalities:  
- Add new book  
- Delete book  
- Search for book  
- Lend book  
- Print book details  
- Print all books with details in library  

**Requirements**
- SBT 1.3.4

**How to compile**
- _sbt compile_

**How to run tests**
- _sbt test_

**How to use**  
All listed above functionalities can be used using two classes, other classes have restricted scope.  

For managing library state please use: _Library Mediator_  
This class is responsible for adding, deleting, searching and lending books.  

For printing library details please use: _LibraryVisualizer_  
Every method exposed by above two classes are self-explaining, should not be any problem with understanding.


