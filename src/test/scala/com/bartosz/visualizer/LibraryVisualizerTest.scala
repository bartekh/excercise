package com.bartosz.visualizer

import java.time.Year

import com.bartosz.common.MockLibraryRepositoryFacade
import com.bartosz.model.Book
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

class LibraryVisualizerTest extends FunSuite with BeforeAndAfterEach with BeforeAndAfterAll {

  private val libraryVisualizer = new LibraryVisualizer with MockLibraryOutput

  test("Print all books from library with details") {
    val expectedOutputWords = List("Books", "available", "in", "library:", "ID.", "Title", "Year", "Author", "1L",
      "title", "2019", "author", "All", "books", "in", "library:", "1", "All", "available", "books", "in", "library:",
      "0", "All", "lent", "books:", "1")

    libraryVisualizer.listAllBooks()
    assert(libraryVisualizer.messages.toString().split("\\s+").map(_.trim).toList == expectedOutputWords)
  }

  test("Print book details") {
    val expectedOutputWords = List("Found", "book", "in", "library:", "ID.", "Title", "Year", "Author", "1L", "title",
      "2019", "author", "This", "book", "is", "currently", "lent", "by:", "clientName")

    libraryVisualizer.getBookDetails("1L")
    assert(libraryVisualizer.messages.toString().split("\\s+").map(_.trim).toList == expectedOutputWords)
  }

  override protected def beforeEach(): Unit = {
    libraryVisualizer.messages.clear()
    libraryVisualizer.libraryRepositoryFacade.add(Book("1L", "title", Year.now(), "author"))
    libraryVisualizer.libraryRepositoryFacade.lend("1L", "clientName")
  }

  override protected def afterEach(): Unit = {
    libraryVisualizer.libraryRepositoryFacade.books.clear()
    libraryVisualizer.libraryRepositoryFacade.lentBooks.clear()
  }
}

trait MockLibraryOutput extends LibraryOutput {
  var messages: StringBuilder = new StringBuilder

  override def print(s: StringBuilder) = messages = messages append s.toString()

  override private[visualizer] lazy val libraryRepositoryFacade = new MockLibraryRepositoryFacade()
}