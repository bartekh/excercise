package com.bartosz.mediator

import java.time.Year
import java.util.UUID

import com.bartosz.common.MockLibraryRepositoryFacade
import com.bartosz.repository.NotExistedBookInLibraryException
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

class LibraryMediatorTest extends FunSuite with BeforeAndAfterEach with BeforeAndAfterAll {

  private val libraryMediator = new LibraryMediator with MockLibraryMediatorTrait
  private val libraryRepositoryFacade = libraryMediator.libraryRepositoryFacade

  override protected def beforeEach(): Unit = libraryRepositoryFacade.books.clear()

  test("Add new book to library succeed") {
    val title = "title"
    val year = Year.now()
    val author = "author"

    libraryMediator.add(title, year, author)

    assert(libraryRepositoryFacade.books.nonEmpty)
    val bookFromLibrary = libraryRepositoryFacade.books.head
    assert(bookFromLibrary.title == title)
    assert(bookFromLibrary.year == year)
    assert(bookFromLibrary.author == author)
  }

  test("Remove book from library succeed") {
    val title = "title"
    val year = Year.now()
    val author = "author"

    libraryMediator.add(title, year, author)
    libraryMediator.remove(libraryRepositoryFacade.books.head.id)

    assert(libraryRepositoryFacade.books.isEmpty)
  }

  test("Lend book from library succeed") {
    val title = "title"
    val year = Year.now()
    val author = "author"

    libraryMediator.add(title, year, author)
    libraryMediator.lend(libraryRepositoryFacade.books.head.id, "clientName")

    assert(libraryRepositoryFacade.books.nonEmpty)
    assert(libraryRepositoryFacade.books.head.title == title)
    assert(libraryRepositoryFacade.lentBooks.nonEmpty)
  }

  test("Lend not existed book from library throws NotExistedBookInLibraryException") {
    val title = "title"
    val year = Year.now()
    val author = "author"

    libraryMediator.add(title, year, author)

    assertThrows[NotExistedBookInLibraryException] {
      libraryRepositoryFacade.lend(UUID.randomUUID().toString, "clientName")
    }
  }

  test("Lend already lent book from library returns None") {
    val title = "title"
    val year = Year.now()
    val author = "author"

    libraryMediator.add(title, year, author)

    val bookId = libraryRepositoryFacade.books.head.id
    libraryMediator.lend(bookId, "clientName")
    assert(libraryMediator.lend(bookId, "clientName").isEmpty)
  }

  test("Find book by title in library succeed") {
    val title = "title1"
    val year = Year.now()
    val author = "author"

    libraryMediator.add(title, year, author)
    libraryMediator.add("title2", year, author)
    val foundBook = libraryMediator.findByTitle("title1")

    assert(foundBook.nonEmpty)
    assert(foundBook.length == 1)
    assert(foundBook.head.title == title)
    assert(foundBook.head.year == year)
    assert(foundBook.head.author == author)
  }

  test("Find book by author in library succeed") {
    val title = "title1"
    val year = Year.now()
    val author = "author1"

    libraryMediator.add(title, year, author)
    libraryMediator.add("title2", year, "author2")
    val foundBook = libraryMediator.findByAuthor("author1")

    assert(foundBook.nonEmpty)
    assert(foundBook.length == 1)
    assert(foundBook.head.title == title)
    assert(foundBook.head.year == year)
    assert(foundBook.head.author == author)
  }

  test("Find book by year in library succeed") {
    val title = "title1"
    val year = Year.now()
    val author = "author1"

    libraryMediator.add(title, year, author)
    libraryMediator.add("title2", year, "author2")
    val foundBook = libraryMediator.findByYear(year)

    assert(foundBook.nonEmpty)
    assert(foundBook.length == 2)
  }

  test("Find book by title and author in library succeed") {
    val title = "title1"
    val year = Year.now()
    val author = "author1"

    libraryMediator.add(title, year, author)
    libraryMediator.add("title2", year, "author2")
    val foundBook = libraryMediator.findByTitleAndAuthor(title, author)

    assert(foundBook.nonEmpty)
    assert(foundBook.length == 1)
    assert(foundBook.head.title == title)
    assert(foundBook.head.year == year)
    assert(foundBook.head.author == author)
  }

  test("Find book by title and year in library succeed") {
    val title = "title1"
    val year = Year.now()
    val author = "author1"

    libraryMediator.add(title, year, author)
    libraryMediator.add("title2", year, "author2")
    val foundBook = libraryMediator.findByTitleAndYear(title, year)

    assert(foundBook.nonEmpty)
    assert(foundBook.length == 1)
    assert(foundBook.head.title == title)
    assert(foundBook.head.year == year)
    assert(foundBook.head.author == author)
  }

  test("Find book by author and year in library succeed") {
    val title = "title1"
    val year = Year.now()
    val author = "author1"

    libraryMediator.add(title, year, author)
    libraryMediator.add("title2", year, "author2")
    val foundBook = libraryMediator.findByAuthorAndYear(author, year)

    assert(foundBook.nonEmpty)
    assert(foundBook.length == 1)
    assert(foundBook.head.title == title)
    assert(foundBook.head.year == year)
    assert(foundBook.head.author == author)
  }
}

trait MockLibraryMediatorTrait extends LibraryMediatorTrait {
  override private[mediator] lazy val libraryRepositoryFacade = new MockLibraryRepositoryFacade()
}