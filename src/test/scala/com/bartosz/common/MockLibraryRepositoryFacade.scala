package com.bartosz.common

import com.bartosz.model.{Book, LentBook}
import com.bartosz.repository.LibraryRepositoryFacade

import scala.collection.mutable.ListBuffer

class MockLibraryRepositoryFacade extends LibraryRepositoryFacade {

  override private[bartosz] val books = new ListBuffer[Book]()
  override private[bartosz] val lentBooks = new ListBuffer[LentBook]()
}
