package com.bartosz.repository

import java.time.Year
import java.util.UUID

import com.bartosz.model.Book
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

class LibraryRepositoryFacadeTest extends FunSuite with BeforeAndAfterEach with BeforeAndAfterAll {

  override protected def afterEach(): Unit = {
    LibraryRepository.books.clear()
    LibraryRepository.lentBooks.clear()
  }

  override protected def beforeAll(): Unit = {
    LibraryRepository.books.clear()
    LibraryRepository.lentBooks.clear()
  }

  private val libraryRepositoryFacade = new LibraryRepositoryFacade()
  test("Add book to repository") {
    val book = Book("1L", "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)

    assert(LibraryRepository.books.head == book)
  }

  test("Remove book from repository") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    val removedBook = libraryRepositoryFacade.remove(bookId)

    assert(LibraryRepository.books.isEmpty)
    assert(removedBook.nonEmpty)
    assert(removedBook.get == book)
  }

  test("Remove not existed book from repository throws NotExistedBookInLibraryException") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    assertThrows[NotExistedBookInLibraryException] {
      libraryRepositoryFacade.remove(UUID.randomUUID().toString)
    }
  }

  test("Remove lent book from repository does not affect book storage") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    libraryRepositoryFacade.lend(bookId, "clientName")
    val removedBook = libraryRepositoryFacade.remove(bookId)

    assert(LibraryRepository.books.nonEmpty)
    assert(removedBook.isEmpty)
    assert(LibraryRepository.lentBooks.nonEmpty)
  }

  test("Added book exists in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)

    assert(libraryRepositoryFacade.exists(bookId))
  }

  test("Added book can be found in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)

    assert(libraryRepositoryFacade.find(bookId).nonEmpty)
    assert(libraryRepositoryFacade.find(bookId).get == book)
  }

  test("Exists function is able to check book in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)

    assert(libraryRepositoryFacade.exists(bookId))
  }

  test("Exists function does not find not existed book in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)

    assert(!libraryRepositoryFacade.exists("1L"))
  }

  test("Finding by id is possible to return book from library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    val foundBook = libraryRepositoryFacade.find(bookId)

    assert(foundBook.nonEmpty)
    assert(foundBook.get == book)
  }

  test("Finding by id returns None for not existed book from library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    val foundBook = libraryRepositoryFacade.find("1L")

    assert(foundBook.isEmpty)
  }

  test("Finding lent book by id is possible to from library") {
    val bookId = UUID.randomUUID().toString
    val clientName = "clientName"
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    libraryRepositoryFacade.lend(bookId, clientName)
    val foundBook = libraryRepositoryFacade.findLentBook(bookId)

    assert(foundBook.nonEmpty)
    assert(foundBook.get.book == book)
    assert(foundBook.get.name == clientName)
  }

  test("Finding lent book by id returns None for not existed lent book in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    libraryRepositoryFacade.lend(bookId, "clientNAme")
    val foundBook = libraryRepositoryFacade.findLentBook("1L")

    assert(foundBook.isEmpty)
  }

  test("Return all books in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    val allBooksInLibrary = libraryRepositoryFacade.getAllBooks()

    assert(allBooksInLibrary.nonEmpty)
    assert(allBooksInLibrary.length == 1)
    assert(allBooksInLibrary.head == book)
  }

  test("Return all lent books in library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")
    val clientName = "clientName"

    libraryRepositoryFacade.add(book)
    libraryRepositoryFacade.lend(bookId, clientName)
    val allLentBooksInLibrary = libraryRepositoryFacade.getAllLentBooks()

    assert(allLentBooksInLibrary.nonEmpty)
    assert(allLentBooksInLibrary.length == 1)
    assert(allLentBooksInLibrary.head.book == book)
    assert(allLentBooksInLibrary.head.name == clientName)
  }

  test("Filtering by id returns existed books from library") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)

    val foundBooks = libraryRepositoryFacade.filter({ case Book(i, _, _, _) => i == bookId })

    assert(foundBooks.length == 1)
    assert(foundBooks.head == book)
  }

  test("Filtering by year returns existed books from library") {
    val year = Year.now()
    val book1 = Book(UUID.randomUUID().toString, "title", year, "author")
    val book2 = Book(UUID.randomUUID().toString, "title", year, "author")

    libraryRepositoryFacade.add(book1)
    libraryRepositoryFacade.add(book2)

    val foundBooks = libraryRepositoryFacade.filter({ case Book(_, _, y, _) => y == year })

    assert(foundBooks.length == 2)
    assert(foundBooks.head == book1)
    assert(foundBooks.tail.head == book2)
  }

  test("Lend book succeed") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    val lentBook = libraryRepositoryFacade.lend(bookId, "clientName")

    assert(LibraryRepository.lentBooks.nonEmpty)
    assert(LibraryRepository.lentBooks.length == 1)
    assert(lentBook.nonEmpty)
    assert(lentBook.get == book)
  }

  test("Lend not existed book from repository throws NotExistedBookInLibraryException") {
    assertThrows[NotExistedBookInLibraryException] {
      libraryRepositoryFacade.lend(UUID.randomUUID().toString, "clientName")
    }
  }

  test("Lent already lend book returns None") {
    val bookId = UUID.randomUUID().toString
    val book = Book(bookId, "title", Year.now(), "author")

    libraryRepositoryFacade.add(book)
    libraryRepositoryFacade.lend(bookId, "clientName1")
    val lentBook = libraryRepositoryFacade.lend(bookId, "clientName2")

    assert(LibraryRepository.lentBooks.nonEmpty)
    assert(LibraryRepository.lentBooks.length == 1)
    assert(lentBook.isEmpty)
  }

  test("Lent not existed book throws NotExistedBookInLibraryException") {
    val bookId = UUID.randomUUID().toString

    assertThrows[NotExistedBookInLibraryException] {
      libraryRepositoryFacade.lend(bookId, "clientName")
    }
  }
}