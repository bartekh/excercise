package com.bartosz.visualizer

import com.bartosz.repository.LibraryRepositoryFacade

class LibraryVisualizer extends LibraryOutput {

  private lazy val header = "ID.\t\t\t\t\t\t\t\t\t Title\t Year\t Author \n"

  def listAllBooks(): Unit = {
    val resultString = new StringBuilder()
    resultString.append(s"Books available in library:\n $header")
    val books = libraryRepositoryFacade.getAllBooks()
    val lentBooks = libraryRepositoryFacade.getAllLentBooks()
    books.foreach(book => resultString.append(s"${book.id}\t ${book.title}\t ${book.year}\t ${book.author}\n"))
    resultString.append(s"\nAll books in library: ${books.length}\n")
    resultString.append(s"All available books in library: ${books.length - lentBooks.length}\n")
    resultString.append(s"All lent books: ${lentBooks.length}")
    print(resultString)
  }

  def getBookDetails(id: String) = {
    val resultString = new StringBuilder()
    libraryRepositoryFacade.find(id) match {
      case None => resultString.append(s"There is no book with provided ID: $id")
      case Some(foundBook) => {
        resultString.append(s"Found book in library:\n $header" +
          s"${foundBook.id}\t ${foundBook.title}\t ${foundBook.year}\t ${foundBook.author}\n")
        libraryRepositoryFacade.findLentBook(foundBook.id) match {
          case None => resultString.append("This book is currently free.")
          case Some(lentBook) => resultString.append(s"This book is currently lent by: ${lentBook.name}")
        }
      }
    }
    print(resultString)
  }
}

trait LibraryOutput {
  def print(s: StringBuilder) = Console.println(s)
  private[visualizer] lazy val libraryRepositoryFacade = new LibraryRepositoryFacade()
}
