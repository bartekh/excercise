package com.bartosz.repository

import com.bartosz.model.{Book, LentBook}

import scala.collection.mutable.ListBuffer

class LibraryRepositoryFacade {

  private[bartosz] val books: ListBuffer[Book] = LibraryRepository.books
  private[bartosz] val lentBooks: ListBuffer[LentBook] = LibraryRepository.lentBooks

  private[bartosz] def add(book: Book): Unit = {
    books += book
  }

  @throws(classOf[NotExistedBookInLibraryException])
  private[bartosz] def remove(id: String): Option[Book] = {
    books.find { case Book(i, _, _, _) => i == id } match {
      case Some(foundBook) =>
        if (isBookLent(foundBook)) None
        else {
          books -= foundBook
          Some(foundBook)
        }
      case None => throw new NotExistedBookInLibraryException(id)
    }
  }

  private[bartosz] def exists(id: String): Boolean = books.exists({ case Book(i, _, _, _) => i == id })

  private[bartosz] def find(id: String): Option[Book] = books.find({ case Book(i, _, _, _) => i == id })

  private[bartosz] def findLentBook(id: String): Option[LentBook] = lentBooks.
    find({ case LentBook(Book(i, _, _, _), _) => i == id })

  private[bartosz] def filter(predicate: Book => Boolean): List[Book] = books.filter(predicate).toList

  private[bartosz] def getAllBooks(): List[Book] = books.toList

  private[bartosz] def getAllLentBooks(): List[LentBook] = lentBooks.toList

  @throws(classOf[NotExistedBookInLibraryException])
  private[bartosz] def lend(id: String, clientName: String): Option[Book] = find(id) match {
    case Some(book) =>
      if (isBookLent(book)) {
        None
      } else {
        lentBooks += LentBook(book, clientName)
        Some(book)
      }
    case None => throw new NotExistedBookInLibraryException(id)
  }

  private def isBookLent(book: Book) = {
    lentBooks.exists({ case LentBook(b, _) => book == b })
  }
}