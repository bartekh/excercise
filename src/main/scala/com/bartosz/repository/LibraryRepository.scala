package com.bartosz.repository

import com.bartosz.model.{Book, LentBook}

import scala.collection.mutable.ListBuffer

object LibraryRepository {
  private[repository] lazy val books = ListBuffer[Book]()
  private[repository] lazy val lentBooks = ListBuffer[LentBook]()
}
