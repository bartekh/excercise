package com.bartosz.repository

class NotExistedBookInLibraryException(id: String) extends Exception {
  override def getMessage: String = "Following book does not exists in library, id: " + id
}
