package com.bartosz.mediator

import java.time.Year
import java.util.UUID

import com.bartosz.model.Book
import com.bartosz.repository.LibraryRepositoryFacade

class LibraryMediator extends LibraryMediatorTrait {

  def add(title: String, year: Year, author: String) =
    libraryRepositoryFacade.add(Book(UUID.randomUUID().toString, title, year, author))

  def remove(id: String) = libraryRepositoryFacade.remove(id)

  def lend(id: String, clientName: String) = libraryRepositoryFacade.lend(id, clientName)

  def findByTitle(title: String): List[Book] = libraryRepositoryFacade.filter({ case Book(_, t, _, _) => title == t })

  def findByAuthor(author: String): List[Book] = libraryRepositoryFacade.filter({ case Book(_, _, _, a) => author == a })

  def findByYear(year: Year): List[Book] = libraryRepositoryFacade.filter({ case Book(_, _, y, _) => year == y })

  def findByTitleAndAuthor(title: String, author: String): List[Book] =
    libraryRepositoryFacade.filter({ case Book(_, t, _, a) => title == t && author == a })

  def findByTitleAndYear(title: String, year: Year): List[Book] =
    libraryRepositoryFacade.filter({ case Book(_, t, y, _) => title == t && year == y })

  def findByAuthorAndYear(author: String, year: Year): List[Book] =
    libraryRepositoryFacade.filter({ case Book(_, _, y, a) => author == a && year == y })
}

trait LibraryMediatorTrait {
  private[mediator] lazy val libraryRepositoryFacade = new LibraryRepositoryFacade()
}