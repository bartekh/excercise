package com.bartosz.model

import java.time.Year

case class Book(id: String, title: String, year: Year, author: String)

